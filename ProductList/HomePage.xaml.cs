﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using ProductList.models;
using Xamarin.Forms;

namespace ProductList
{
    public partial class HomePage : ContentPage
    {
        private ObservableCollection<Product> _productList;

        public HomePage()
        {
            InitializeComponent();

            PopulateAnimeList();
        }

        void HandleDeleteClicked(object sender, System.EventArgs e)
        {
            if (!(sender is MenuItem item) || item.CommandParameter == null)
            {
                return;
            }

            RemoveItemById((long)item.CommandParameter);
        }

        void HandleCellTapped(object sender, System.EventArgs e)
        {
            if (!(sender is ViewCell item) || item.BindingContext == null)
            {
                return;
            }

            var product = item.BindingContext as Product;
            RemoveItemById(product.ProductId);
        }

        void HandleRefreshing(object sender, System.EventArgs e)
        {
            PopulateAnimeList();
            ProductListView.IsRefreshing = false;
        }

        async void HandleNavigateToProductPage(object sender, EventArgs args)
        {
            if (!(sender is MenuItem item) || item.BindingContext == null)
            {
                return;
            }

            var product = item.BindingContext as Product;
            await Navigation.PushAsync(new ProductPage(product));
        }

        private void RemoveItemById(long id)
        {
            Product item = (from itm in _productList where itm.ProductId == id select itm)
                             .FirstOrDefault<Product>();
            _productList.Remove(item);
        }

        private ObservableCollection<Product> GetProductList()
        {
            try
            {
                const string fileName = "ProductList.products.json";

                var assembly = typeof(HomePage).GetTypeInfo().Assembly;
                Stream stream = assembly.GetManifestResourceStream(fileName);
                using (var reader = new System.IO.StreamReader(stream))
                {
                    var jsonAsString = reader.ReadToEnd();
                    return new ObservableCollection<Product>(Product.FromJson(jsonAsString));
                }
            }
            catch (IOException)
            {
                return new ObservableCollection<Product>();
            }
        }

        private void PopulateAnimeList()
        {
            _productList = GetProductList();
            ProductListView.ItemsSource = _productList;
        }
    }
}
