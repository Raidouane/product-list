﻿using System;
using System.Collections.Generic;
using ProductList.models;
using Xamarin.Forms;

namespace ProductList
{
    public partial class ProductPage : ContentPage
    {
        public ProductPage(Product product)
        {
            InitializeComponent();
            BindingContext = product;
        }
    }
}
